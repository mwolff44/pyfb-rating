.. :changelog:

History
-------

1.0.0 (2020-05-21)
++++++++++++++++++

* stable version

0.11.3 (2019-06-17)
+++++++++++++++++++

* Bug : remove unused view (cause issue in some setup).

0.11.2 (2019-05-20)
+++++++++++++++++++

* Bug : correct discount model.

0.11.0 (2018-12-21)
+++++++++++++++++++

* Add ratecard allocation to customer.

0.10.0 (2018-12-20)
+++++++++++++++++++

* Some corrections.

0.9.0 (2018-12-05)
++++++++++++++++++

* First release on PyPI.
