=====
Usage
=====

To use pyfb-rating in a project, add it to your `INSTALLED_APPS`:

.. code-block:: python

    INSTALLED_APPS = (
        ...
        'pyfb_rating.apps.PyfbRatingConfig',
        ...
    )

Add pyfb-rating's URL patterns:

.. code-block:: python

    from pyfb_rating import urls as pyfb_rating_urls


    urlpatterns = [
        ...
        url(r'^', include(pyfb_rating_urls)),
        ...
    ]
