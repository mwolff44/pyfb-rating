============
Installation
============

At the command line::

    $ easy_install pyfb-rating

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv pyfb-rating
    $ pip install pyfb-rating
