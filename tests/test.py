#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.test import TestCase
from django.urls import reverse
from django.test import Client

from django.template.defaultfilters import slugify

from pyfb_direction.models import Carrier, Type, Region, Country, Destination, Prefix

from pyfb_rating.models import CustomerRcAllocation, CallerNumList, ProviderRatecard, CustomerRatecard, CustomerPrefixRate, ProviderPrefixRate, CustomerDestinationRate, ProviderDestinationRate, CustomerCountryTypeRate, ProviderCountryTypeRate, CustomerCountryRate, ProviderCountryRate, CustomerRegionTypeRate, ProviderRegionTypeRate, CustomerRegionRate, ProviderRegionRate, CustomerDefaultRate, ProviderDefaultRate


def create_customerrcallocation(**kwargs):
    defaults = {}
    defaults["tech_prefix"] = "tech_prefix"
    defaults["priority"] = "priority"
    defaults["discount"] = "discount"
    defaults["allow_negative_margin"] = "allow_negative_margin"
    defaults["description"] = "description"
    defaults.update(**kwargs)
    if "customer" not in defaults:
        defaults["customer"] = create_customer()
    if "ratecard" not in defaults:
        defaults["ratecard"] = create_customerratecard()
    return CustomerRcAllocation.objects.create(**defaults)


def create_carrier(**kwargs):
    defaults = {}
    defaults["name"] = "name"
    defaults["slug"] = slugify("name")
    defaults.update(**kwargs)
    return Carrier.objects.create(**defaults)


def create_type(**kwargs):
    defaults = {}
    defaults["name"] = "name"
    defaults["slug"] = slugify("name")
    defaults.update(**kwargs)
    return Type.objects.create(**defaults)


def create_region(**kwargs):
    defaults = {}
    defaults["name"] = "name"
    defaults["slug"] = slugify("name")
    defaults.update(**kwargs)
    return Region.objects.create(**defaults)


def create_country(**kwargs):
    defaults = {}
    defaults["country_iso2"] = "FR"
    defaults.update(**kwargs)
    if "region" not in defaults:
        defaults["region"] = create_region()
    return Country.objects.create(**defaults)


def create_destination(**kwargs):
    defaults = {}
    defaults["name"] = "name"
    defaults["slug"] = slugify("name")
    defaults.update(**kwargs)
    if "country_iso2" not in defaults:
        defaults["country_iso2"] = create_country()
    if "carrier" not in defaults:
        defaults["carrier"] = create_carrier()
    if "type" not in defaults:
        defaults["type"] = create_type()
    return Destination.objects.create(**defaults)


def create_callernumlist(**kwargs):
    defaults = {}
    defaults["name"] = "name"
    defaults["slug"] = slugify("name")
    defaults["callerid_filter"] = "1"
    defaults.update(**kwargs)
    # if "destination" not in defaults:
    #     defaults["destination"] = create_destination()
    return CallerNumList.objects.create(**defaults)


def create_providerratecard(**kwargs):
    defaults = {}
    defaults["name"] = "name"
    defaults["slug"] = slugify("name")
    defaults["rc_type"] = "pstn"
    defaults["provider_prefix"] = ""
    defaults["estimated_quality"] = 10
    defaults["status"] = "enabled"
    defaults["description"] = "description"
    defaults["date_start"] = '2013-11-11'
    defaults["date_end"] = '2013-11-11'
    defaults.update(**kwargs)
    if "callerid_list" not in defaults:
        defaults["callerid_list"] = create_callernumlist()
    return ProviderRatecard.objects.create(**defaults)


def create_customerratecard(**kwargs):
    defaults = {}
    defaults["slug"] = slugify("name")
    defaults["rc_type"] = "pstn"
    defaults["name"] = "name"
    defaults["status"] = "enabled"
    defaults["description"] = "description"
    defaults["date_start"] = '2013-11-11'
    defaults["date_end"] = '2013-11-11'
    defaults.update(**kwargs)
    if "callerid_list" not in defaults:
        defaults["callerid_list"] = create_callernumlist()
    return CustomerRatecard.objects.create(**defaults)


def create_customerprefixrate(**kwargs):
    defaults = {}
    defaults["prefix"] = "prefix"
    defaults["destnum_length"] = 5
    defaults["r_rate"] = '1.5'
    defaults["r_block_min_duration"] = 1
    defaults["r_minimal_time"] = 1
    defaults["r_init_block"] = '1.5'
    defaults["status"] = "enabled"
    defaults.update(**kwargs)
    if "c_ratecard" not in defaults:
        defaults["c_ratecard"] = create_customerratecard()
    return CustomerPrefixRate.objects.create(**defaults)


def create_providerprefixrate(**kwargs):
    defaults = {}
    defaults["prefix"] = "prefix"
    defaults["destnum_length"] = 5
    defaults["r_rate"] = '1.5'
    defaults["r_block_min_duration"] = 1
    defaults["r_minimal_time"] = 1
    defaults["r_init_block"] = '1.5'
    defaults["status"] = "enabled"
    defaults.update(**kwargs)
    if "p_ratecard" not in defaults:
        defaults["p_ratecard"] = create_providerratecard()
    return ProviderPrefixRate.objects.create(**defaults)


def create_customerdestinationrate(**kwargs):
    defaults = {}
    defaults["r_rate"] = '1.5'
    defaults["r_block_min_duration"] = 1
    defaults["r_minimal_time"] = 1
    defaults["r_init_block"] = '1.5'
    defaults["status"] = "enabled"
    defaults.update(**kwargs)
    if "c_ratecard" not in defaults:
        defaults["c_ratecard"] = create_customerratecard()
    if "destination" not in defaults:
        defaults["destination"] = create_destination()
    return CustomerDestinationRate.objects.create(**defaults)


def create_providerdestinationrate(**kwargs):
    defaults = {}
    defaults["r_rate"] = '1.5'
    defaults["r_block_min_duration"] = 1
    defaults["r_minimal_time"] = 1
    defaults["r_init_block"] = '1.5'
    defaults["status"] = "enabled"
    defaults.update(**kwargs)
    if "p_ratecard" not in defaults:
        defaults["p_ratecard"] = create_providerratecard()
    if "destination" not in defaults:
        defaults["destination"] = create_destination()
    return ProviderDestinationRate.objects.create(**defaults)


def create_customercountrytyperate(**kwargs):
    defaults = {}
    defaults["r_rate"] = '1.5'
    defaults["r_block_min_duration"] = 1
    defaults["r_minimal_time"] = 1
    defaults["r_init_block"] = '1.5'
    defaults["status"] = "enabled"
    defaults.update(**kwargs)
    if "c_ratecard" not in defaults:
        defaults["c_ratecard"] = create_customerratecard()
    if "country" not in defaults:
        defaults["country"] = create_country()
    if "type" not in defaults:
        defaults["type"] = create_type()
    return CustomerCountryTypeRate.objects.create(**defaults)


def create_providercountrytyperate(**kwargs):
    defaults = {}
    defaults["r_rate"] = '1.5'
    defaults["r_block_min_duration"] = 1
    defaults["r_minimal_time"] = 1
    defaults["r_init_block"] = '1.5'
    defaults["status"] = "enabled"
    defaults.update(**kwargs)
    if "p_ratecard" not in defaults:
        defaults["p_ratecard"] = create_providerratecard()
    if "country" not in defaults:
        defaults["country"] = create_country()
    if "type" not in defaults:
        defaults["type"] = create_type()
    return ProviderCountryTypeRate.objects.create(**defaults)


def create_customercountryrate(**kwargs):
    defaults = {}
    defaults["r_rate"] = '1.5'
    defaults["r_block_min_duration"] = 1
    defaults["r_minimal_time"] = 1
    defaults["r_init_block"] = '1.5'
    defaults["status"] = "enabled"
    defaults.update(**kwargs)
    if "c_ratecard" not in defaults:
        defaults["c_ratecard"] = create_customerratecard()
    if "country" not in defaults:
        defaults["country"] = create_country()
    return CustomerCountryRate.objects.create(**defaults)


def create_providercountryrate(**kwargs):
    defaults = {}
    defaults["r_rate"] = '1.5'
    defaults["r_block_min_duration"] = 1
    defaults["r_minimal_time"] = 1
    defaults["r_init_block"] = '1.5'
    defaults["status"] = "enabled"
    defaults.update(**kwargs)
    if "p_ratecard" not in defaults:
        defaults["p_ratecard"] = create_providerratecard()
    if "country" not in defaults:
        defaults["country"] = create_country()
    return ProviderCountryRate.objects.create(**defaults)


def create_customerregiontyperate(**kwargs):
    defaults = {}
    defaults["r_rate"] = '1.5'
    defaults["r_block_min_duration"] = 1
    defaults["r_minimal_time"] = 1
    defaults["r_init_block"] = '1.5'
    defaults["status"] = "enabled"
    defaults.update(**kwargs)
    if "c_ratecard" not in defaults:
        defaults["c_ratecard"] = create_customerratecard()
    if "region" not in defaults:
        defaults["region"] = create_region()
    if "type" not in defaults:
        defaults["type"] = create_type()
    return CustomerRegionTypeRate.objects.create(**defaults)


def create_providerregiontyperate(**kwargs):
    defaults = {}
    defaults["r_rate"] = '1.5'
    defaults["r_block_min_duration"] = 1
    defaults["r_minimal_time"] = 1
    defaults["r_init_block"] = '1.5'
    defaults["status"] = "enabled"
    defaults.update(**kwargs)
    if "p_ratecard" not in defaults:
        defaults["p_ratecard"] = create_providerratecard()
    if "region" not in defaults:
        defaults["region"] = create_region()
    if "type" not in defaults:
        defaults["type"] = create_type()
    return ProviderRegionTypeRate.objects.create(**defaults)


def create_customerregionrate(**kwargs):
    defaults = {}
    defaults["r_rate"] = '1.5'
    defaults["r_block_min_duration"] = 1
    defaults["r_minimal_time"] = 1
    defaults["r_init_block"] = '1.5'
    defaults["status"] = "enabled"
    defaults.update(**kwargs)
    if "c_ratecard" not in defaults:
        defaults["c_ratecard"] = create_customerratecard()
    if "region" not in defaults:
        defaults["region"] = create_region()
    return CustomerRegionRate.objects.create(**defaults)


def create_providerregionrate(**kwargs):
    defaults = {}
    defaults["r_rate"] = '1.5'
    defaults["r_block_min_duration"] = 1
    defaults["r_minimal_time"] = 1
    defaults["r_init_block"] = '1.5'
    defaults["status"] = "enabled"
    defaults.update(**kwargs)
    if "p_ratecard" not in defaults:
        defaults["p_ratecard"] = create_providerratecard()
    if "region" not in defaults:
        defaults["region"] = create_region()
    return ProviderRegionRate.objects.create(**defaults)


def create_customerdefaultrate(**kwargs):
    defaults = {}
    defaults["r_rate"] = '1.5'
    defaults["r_block_min_duration"] = 1
    defaults["r_minimal_time"] = 1
    defaults["r_init_block"] = '1.5'
    defaults["status"] = "enabled"
    defaults.update(**kwargs)
    if "c_ratecard" not in defaults:
        defaults["c_ratecard"] = create_customerratecard()
    return CustomerDefaultRate.objects.create(**defaults)


def create_providerdefaultrate(**kwargs):
    defaults = {}
    defaults["r_rate"] = '1.5'
    defaults["r_block_min_duration"] = 1
    defaults["r_minimal_time"] = 1
    defaults["r_init_block"] = '1.5'
    defaults["status"] = "enabled"
    defaults.update(**kwargs)
    if "p_ratecard" not in defaults:
        defaults["p_ratecard"] = create_providerratecard()
    return ProviderDefaultRate.objects.create(**defaults)


class CustomerRcAllocationViewTest(TestCase):
    '''
    Tests for CustomerRcAllocation
    '''
    def setUp(self):
        self.client = Client()

    def test_list_customerrcallocation(self):
        url = reverse('pyfb-rating:pyfb_rating_customerrcallocation_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_customerrcallocation(self):
        url = reverse('pyfb-rating:pyfb_rating_customerrcallocation_create')
        data = {
            "tech_prefix": "tech_prefix",
            "priority": "priority",
            "discount": "discount",
            "allow_negative_margin": "allow_negative_margin",
            "description": "description",
            "customer": create_customer().pk,
            "ratecard": create_customerratecard().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_customerrcallocation(self):
        customerrcallocation = create_customerrcallocation()
        url = reverse('pyfb-rating:pyfb_rating_customerrcallocation_detail', args=[customerrcallocation.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_customerrcallocation(self):
        customerrcallocation = create_customerrcallocation()
        data = {
            "tech_prefix": "tech_prefix",
            "priority": "priority",
            "discount": "discount",
            "allow_negative_margin": "allow_negative_margin",
            "description": "description",
            "customer": create_customer().pk,
            "ratecard": create_customerratecard().pk,
        }
        url = reverse('pyfb-rating:pyfb_rating_customerrcallocation_update', args=[customerrcallocation.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class CallerNumListViewTest(TestCase):
    '''
    Tests for CallerNumList
    '''
    def setUp(self):
        self.client = Client()

    def test_list_callernumlist(self):
        url = reverse('pyfb-rating:pyfb_rating_callernumlist_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_callernumlist(self):
        url = reverse('pyfb-rating:pyfb_rating_callernumlist_create')
        data = {
            "name": "name",
            "slug": slugify("name"),
            "callerid_filter": "1",
            "destination": create_destination().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_callernumlist(self):
        callernumlist = create_callernumlist()
        url = reverse('pyfb-rating:pyfb_rating_callernumlist_detail', args=[callernumlist.slug,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_callernumlist(self):
        callernumlist = create_callernumlist()
        data = {
            "name": "name1",
            "slug": "name1",
            "callerid_filter": "1",
            "destination": create_destination().pk,
        }
        url = reverse('pyfb-rating:pyfb_rating_callernumlist_update', args=[callernumlist.slug,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class ProviderRatecardViewTest(TestCase):
    '''
    Tests for ProviderRatecard
    '''
    def setUp(self):
        self.client = Client()

    def test_list_providerratecard(self):
        url = reverse('pyfb-rating:pyfb_rating_providerratecard_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_providerratecard(self):
        url = reverse('pyfb-rating:pyfb_rating_providerratecard_create')
        data = {
            "name": "name",
            "slug": slugify("name"),
            "rc_type": "pstn",
            "provider_prefix": "",
            "estimated_quality": 10,
            "status": "enabled",
            "status_changed": '2013-11-11',
            "description": "description",
            "date_start": '2013-11-11',
            "date_end": '2013-11-11',
            "callerid_list": 1, # create_callernumlist().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_providerratecard(self):
        providerratecard = create_providerratecard()
        url = reverse('pyfb-rating:pyfb_rating_providerratecard_detail', args=[providerratecard.slug,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_providerratecard(self):
        providerratecard = create_providerratecard()
        data = {
            "name": "name",
            "slug": slugify("name"),
            "rc_type": "pstn",
            "provider_prefix": "1",
            "estimated_quality": 10,
            "status": "enabled",
            "status_changed": '2013-11-11',
            "description": "description",
            "date_start": '2013-11-11',
            "date_end": '2013-11-11',
            "callerid_list": 1,
        }
        url = reverse('pyfb-rating:pyfb_rating_providerratecard_update', args=[providerratecard.slug,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class CustomerRatecardViewTest(TestCase):
    '''
    Tests for CustomerRatecard
    '''
    def setUp(self):
        self.client = Client()

    def test_list_customerratecard(self):
        url = reverse('pyfb-rating:pyfb_rating_customerratecard_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_customerratecard(self):
        url = reverse('pyfb-rating:pyfb_rating_customerratecard_create')
        data = {
            "slug": slugify("name"),
            "rc_type": "pstn",
            "name": "name",
            "status": "enabled",
            "status_changed": '2013-11-11',
            "description": "description",
            "date_start": '2013-11-11',
            "date_end": '2013-11-11',
            "callerid_list": 1,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_customerratecard(self):
        customerratecard = create_customerratecard()
        url = reverse('pyfb-rating:pyfb_rating_customerratecard_detail', args=[customerratecard.slug,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_customerratecard(self):
        customerratecard = create_customerratecard()
        data = {
            "slug": slugify("name"),
            "rc_type": "pstn",
            "name": "name",
            "status": "enabled",
            "status_changed": '2013-11-11',
            "description": "description",
            "date_start": '2013-11-11',
            "date_end": '2013-11-11',
            "callerid_list": 1,
        }
        url = reverse('pyfb-rating:pyfb_rating_customerratecard_update', args=[customerratecard.slug,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class CustomerPrefixRateViewTest(TestCase):
    '''
    Tests for CustomerPrefixRate
    '''
    def setUp(self):
        self.client = Client()

    def test_list_customerprefixrate(self):
        url = reverse('pyfb-rating:pyfb_rating_customerprefixrate_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_customerprefixrate(self):
        url = reverse('pyfb-rating:pyfb_rating_customerprefixrate_create')
        data = {
            "prefix": "prefix",
            "destnum_length": 5,
            "r_rate": '1.5',
            "r_block_min_duration": 1,
            "r_minimal_time": 1,
            "r_init_block": '1.5',
            "status": "enabled",
            "c_ratecard": create_customerratecard().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_customerprefixrate(self):
        customerprefixrate = create_customerprefixrate()
        url = reverse('pyfb-rating:pyfb_rating_customerprefixrate_detail', args=[customerprefixrate.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_customerprefixrate(self):
        customerprefixrate = create_customerprefixrate()
        data = {
            "prefix": "prefix1",
            "destnum_length": 5,
            "r_rate": '1.5',
            "r_block_min_duration": 1,
            "r_minimal_time": 1,
            "r_init_block": '1.5',
            "status": "enabled",
            #"c_ratecard": create_customerratecard().pk,
        }
        url = reverse('pyfb-rating:pyfb_rating_customerprefixrate_update', args=[customerprefixrate.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class ProviderPrefixRateViewTest(TestCase):
    '''
    Tests for ProviderPrefixRate
    '''
    def setUp(self):
        self.client = Client()

    def test_list_providerprefixrate(self):
        url = reverse('pyfb-rating:pyfb_rating_providerprefixrate_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_providerprefixrate(self):
        url = reverse('pyfb-rating:pyfb_rating_providerprefixrate_create')
        data = {
            "prefix": "prefix",
            "destnum_length": 5,
            "r_rate": '1.5',
            "r_block_min_duration": 1,
            "r_minimal_time": 1,
            "r_init_block": '1.5',
            "status": "enabled",
            "p_ratecard": create_providerratecard().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_providerprefixrate(self):
        providerprefixrate = create_providerprefixrate()
        url = reverse('pyfb-rating:pyfb_rating_providerprefixrate_detail', args=[providerprefixrate.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_providerprefixrate(self):
        providerprefixrate = create_providerprefixrate()
        data = {
            "prefix": "prefix",
            "destnum_length": 5,
            "r_rate": '1.5',
            "r_block_min_duration": 1,
            "r_minimal_time": 1,
            "r_init_block": '1.5',
            "status": "enabled",
            "p_ratecard": 1,
        }
        url = reverse('pyfb-rating:pyfb_rating_providerprefixrate_update', args=[providerprefixrate.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class CustomerDestinationRateViewTest(TestCase):
    '''
    Tests for CustomerDestinationRate
    '''
    def setUp(self):
        self.client = Client()

    def test_list_customerdestinationrate(self):
        url = reverse('pyfb-rating:pyfb_rating_customerdestinationrate_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_customerdestinationrate(self):
        url = reverse('pyfb-rating:pyfb_rating_customerdestinationrate_create')
        data = {
            "r_rate": '1.5',
            "r_block_min_duration": 1,
            "r_minimal_time": 1,
            "r_init_block": '1.5',
            "status": "enabled",
            "c_ratecard": create_customerratecard().pk,
            "destination": create_destination().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_customerdestinationrate(self):
        customerdestinationrate = create_customerdestinationrate()
        url = reverse('pyfb-rating:pyfb_rating_customerdestinationrate_detail', args=[customerdestinationrate.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_customerdestinationrate(self):
        customerdestinationrate = create_customerdestinationrate()
        data = {
            "r_rate": '1.5',
            "r_block_min_duration": 1,
            "r_minimal_time": 1,
            "r_init_block": '1.5',
            "status": "enabled",
            "c_ratecard": 1,
            "destination": 1,
        }
        url = reverse('pyfb-rating:pyfb_rating_customerdestinationrate_update', args=[customerdestinationrate.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class ProviderDestinationRateViewTest(TestCase):
    '''
    Tests for ProviderDestinationRate
    '''
    def setUp(self):
        self.client = Client()

    def test_list_providerdestinationrate(self):
        url = reverse('pyfb-rating:pyfb_rating_providerdestinationrate_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_providerdestinationrate(self):
        url = reverse('pyfb-rating:pyfb_rating_providerdestinationrate_create')
        data = {
            "r_rate": '1.5',
            "r_block_min_duration": 1,
            "r_minimal_time": 1,
            "r_init_block": '1.5',
            "status": "enabled",
            "p_ratecard": create_providerratecard().pk,
            "destination": create_destination().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_providerdestinationrate(self):
        providerdestinationrate = create_providerdestinationrate()
        url = reverse('pyfb-rating:pyfb_rating_providerdestinationrate_detail', args=[providerdestinationrate.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_providerdestinationrate(self):
        providerdestinationrate = create_providerdestinationrate()
        data = {
            "r_rate": '1.5',
            "r_block_min_duration": 1,
            "r_minimal_time": 1,
            "r_init_block": '1.5',
            "status": "enabled",
            "p_ratecard": 1,
            "destination": 1,
        }
        url = reverse('pyfb-rating:pyfb_rating_providerdestinationrate_update', args=[providerdestinationrate.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class CustomerCountryTypeRateViewTest(TestCase):
    '''
    Tests for CustomerCountryTypeRate
    '''
    def setUp(self):
        self.client = Client()

    def test_list_customercountrytyperate(self):
        url = reverse('pyfb-rating:pyfb_rating_customercountrytyperate_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_customercountrytyperate(self):
        url = reverse('pyfb-rating:pyfb_rating_customercountrytyperate_create')
        data = {
            "r_rate": '1.5',
            "r_block_min_duration": 1,
            "r_minimal_time": 1,
            "r_init_block": '1.5',
            "status": "enabled",
            "c_ratecard": create_customerratecard().pk,
            "country": create_country().pk,
            "type": create_type().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_customercountrytyperate(self):
        customercountrytyperate = create_customercountrytyperate()
        url = reverse('pyfb-rating:pyfb_rating_customercountrytyperate_detail', args=[customercountrytyperate.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_customercountrytyperate(self):
        customercountrytyperate = create_customercountrytyperate()
        data = {
            "r_rate": '1.5',
            "r_block_min_duration": 1,
            "r_minimal_time": 1,
            "r_init_block": '1.5',
            "status": "enabled",
            "c_ratecard": 1,
            "country": 1,
            "type": 1,
        }
        url = reverse('pyfb-rating:pyfb_rating_customercountrytyperate_update', args=[customercountrytyperate.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class ProviderCountryTypeRateViewTest(TestCase):
    '''
    Tests for ProviderCountryTypeRate
    '''
    def setUp(self):
        self.client = Client()

    def test_list_providercountrytyperate(self):
        url = reverse('pyfb-rating:pyfb_rating_providercountrytyperate_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_providercountrytyperate(self):
        url = reverse('pyfb-rating:pyfb_rating_providercountrytyperate_create')
        data = {
            "r_rate": '1.5',
            "r_block_min_duration": 1,
            "r_minimal_time": 1,
            "r_init_block": '1.5',
            "status": "enabled",
            "p_ratecard": create_providerratecard().pk,
            "country": create_country().pk,
            "type": create_type().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_providercountrytyperate(self):
        providercountrytyperate = create_providercountrytyperate()
        url = reverse('pyfb-rating:pyfb_rating_providercountrytyperate_detail', args=[providercountrytyperate.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_providercountrytyperate(self):
        providercountrytyperate = create_providercountrytyperate()
        data = {
            "r_rate": '1.5',
            "r_block_min_duration": 1,
            "r_minimal_time": 1,
            "r_init_block": '1.5',
            "status": "enabled",
            "p_ratecard": 1,
            "country": 1,
            "type": 1,
        }
        url = reverse('pyfb-rating:pyfb_rating_providercountrytyperate_update', args=[providercountrytyperate.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class CustomerCountryRateViewTest(TestCase):
    '''
    Tests for CustomerCountryRate
    '''
    def setUp(self):
        self.client = Client()

    def test_list_customercountryrate(self):
        url = reverse('pyfb-rating:pyfb_rating_customercountryrate_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_customercountryrate(self):
        url = reverse('pyfb-rating:pyfb_rating_customercountryrate_create')
        data = {
            "r_rate": '1.5',
            "r_block_min_duration": 1,
            "r_minimal_time": 1,
            "r_init_block": '1.5',
            "status": "enabled",
            "c_ratecard": create_customerratecard().pk,
            "country": create_country().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_customercountryrate(self):
        customercountryrate = create_customercountryrate()
        url = reverse('pyfb-rating:pyfb_rating_customercountryrate_detail', args=[customercountryrate.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_customercountryrate(self):
        customercountryrate = create_customercountryrate()
        data = {
            "r_rate": '1.5',
            "r_block_min_duration": 1,
            "r_minimal_time": 1,
            "r_init_block": '1.5',
            "status": "enabled",
            "c_ratecard": 1,
            "country": 1,
        }
        url = reverse('pyfb-rating:pyfb_rating_customercountryrate_update', args=[customercountryrate.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class ProviderCountryRateViewTest(TestCase):
    '''
    Tests for ProviderCountryRate
    '''
    def setUp(self):
        self.client = Client()

    def test_list_providercountryrate(self):
        url = reverse('pyfb-rating:pyfb_rating_providercountryrate_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_providercountryrate(self):
        url = reverse('pyfb-rating:pyfb_rating_providercountryrate_create')
        data = {
            "r_rate": '1.5',
            "r_block_min_duration": 1,
            "r_minimal_time": 1,
            "r_init_block": '1.5',
            "status": "enabled",
            "p_ratecard": create_providerratecard().pk,
            "country": create_country().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_providercountryrate(self):
        providercountryrate = create_providercountryrate()
        url = reverse('pyfb-rating:pyfb_rating_providercountryrate_detail', args=[providercountryrate.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_providercountryrate(self):
        providercountryrate = create_providercountryrate()
        data = {
            "r_rate": '1.5',
            "r_block_min_duration": 1,
            "r_minimal_time": 1,
            "r_init_block": '1.5',
            "status": "enabled",
            "p_ratecard": 1,
            "country": 1,
        }
        url = reverse('pyfb-rating:pyfb_rating_providercountryrate_update', args=[providercountryrate.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class CustomerRegionTypeRateViewTest(TestCase):
    '''
    Tests for CustomerRegionTypeRate
    '''
    def setUp(self):
        self.client = Client()

    def test_list_customerregiontyperate(self):
        url = reverse('pyfb-rating:pyfb_rating_customerregiontyperate_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_customerregiontyperate(self):
        url = reverse('pyfb-rating:pyfb_rating_customerregiontyperate_create')
        data = {
            "r_rate": '1.5',
            "r_block_min_duration": 1,
            "r_minimal_time": 1,
            "r_init_block": '1.5',
            "status": "enabled",
            "c_ratecard": create_customerratecard().pk,
            "region": create_region().pk,
            "type": create_type().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_customerregiontyperate(self):
        customerregiontyperate = create_customerregiontyperate()
        url = reverse('pyfb-rating:pyfb_rating_customerregiontyperate_detail', args=[customerregiontyperate.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_customerregiontyperate(self):
        customerregiontyperate = create_customerregiontyperate()
        data = {
            "r_rate": '1.5',
            "r_block_min_duration": 1,
            "r_minimal_time": 1,
            "r_init_block": '1.5',
            "status": "enabled",
            "c_ratecard": 1,
            "region": 1,
            "type": 1,
        }
        url = reverse('pyfb-rating:pyfb_rating_customerregiontyperate_update', args=[customerregiontyperate.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class ProviderRegionTypeRateViewTest(TestCase):
    '''
    Tests for ProviderRegionTypeRate
    '''
    def setUp(self):
        self.client = Client()

    def test_list_providerregiontyperate(self):
        url = reverse('pyfb-rating:pyfb_rating_providerregiontyperate_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_providerregiontyperate(self):
        url = reverse('pyfb-rating:pyfb_rating_providerregiontyperate_create')
        data = {
            "r_rate": '1.5',
            "r_block_min_duration": 1,
            "r_minimal_time": 1,
            "r_init_block": '1.5',
            "status": "enabled",
            "p_ratecard": create_providerratecard().pk,
            "region": create_region().pk,
            "type": create_type().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_providerregiontyperate(self):
        providerregiontyperate = create_providerregiontyperate()
        url = reverse('pyfb-rating:pyfb_rating_providerregiontyperate_detail', args=[providerregiontyperate.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_providerregiontyperate(self):
        providerregiontyperate = create_providerregiontyperate()
        data = {
            "r_rate": '1.5',
            "r_block_min_duration": 1,
            "r_minimal_time": 1,
            "r_init_block": '1.5',
            "status": "enabled",
            "p_ratecard": 1,
            "region": 1,
            "type": 1,
        }
        url = reverse('pyfb-rating:pyfb_rating_providerregiontyperate_update', args=[providerregiontyperate.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class CustomerRegionRateViewTest(TestCase):
    '''
    Tests for CustomerRegionRate
    '''
    def setUp(self):
        self.client = Client()

    def test_list_customerregionrate(self):
        url = reverse('pyfb-rating:pyfb_rating_customerregionrate_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_customerregionrate(self):
        url = reverse('pyfb-rating:pyfb_rating_customerregionrate_create')
        data = {
            "r_rate": '1.5',
            "r_block_min_duration": 1,
            "r_minimal_time": 1,
            "r_init_block": '1.5',
            "status": "enabled",
            "c_ratecard": create_customerratecard().pk,
            "region": create_region().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_customerregionrate(self):
        customerregionrate = create_customerregionrate()
        url = reverse('pyfb-rating:pyfb_rating_customerregionrate_detail', args=[customerregionrate.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_customerregionrate(self):
        customerregionrate = create_customerregionrate()
        data = {
            "r_rate": '1.5',
            "r_block_min_duration": 1,
            "r_minimal_time": 1,
            "r_init_block": '1.5',
            "status": "enabled",
            "c_ratecard": 1,
            "region": 1,
        }
        url = reverse('pyfb-rating:pyfb_rating_customerregionrate_update', args=[customerregionrate.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class ProviderRegionRateViewTest(TestCase):
    '''
    Tests for ProviderRegionRate
    '''
    def setUp(self):
        self.client = Client()

    def test_list_providerregionrate(self):
        url = reverse('pyfb-rating:pyfb_rating_providerregionrate_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_providerregionrate(self):
        url = reverse('pyfb-rating:pyfb_rating_providerregionrate_create')
        data = {
            "r_rate": '1.5',
            "r_block_min_duration": 1,
            "r_minimal_time": 1,
            "r_init_block": '1.5',
            "status": "enabled",
            "p_ratecard": create_providerratecard().pk,
            "region": create_region().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_providerregionrate(self):
        providerregionrate = create_providerregionrate()
        url = reverse('pyfb-rating:pyfb_rating_providerregionrate_detail', args=[providerregionrate.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_providerregionrate(self):
        providerregionrate = create_providerregionrate()
        data = {
            "r_rate": '1.5',
            "r_block_min_duration": 1,
            "r_minimal_time": 1,
            "r_init_block": '1.5',
            "status": "enabled",
            "p_ratecard": 1,
            "region": 1,
        }
        url = reverse('pyfb-rating:pyfb_rating_providerregionrate_update', args=[providerregionrate.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class CustomerDefaultRateViewTest(TestCase):
    '''
    Tests for CustomerDefaultRate
    '''
    def setUp(self):
        self.client = Client()

    def test_list_customerdefaultrate(self):
        url = reverse('pyfb-rating:pyfb_rating_customerdefaultrate_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_customerdefaultrate(self):
        url = reverse('pyfb-rating:pyfb_rating_customerdefaultrate_create')
        data = {
            "r_rate": '1.5',
            "r_block_min_duration": 1,
            "r_minimal_time": 1,
            "r_init_block": '1.5',
            "status": "enabled",
            "c_ratecard": create_customerratecard().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_customerdefaultrate(self):
        customerdefaultrate = create_customerdefaultrate()
        url = reverse('pyfb-rating:pyfb_rating_customerdefaultrate_detail', args=[customerdefaultrate.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_customerdefaultrate(self):
        customerdefaultrate = create_customerdefaultrate()
        data = {
            "r_rate": '1.5',
            "r_block_min_duration": 1,
            "r_minimal_time": 1,
            "r_init_block": '1.5',
            "status": "enabled",
            "c_ratecard": 1,
        }
        url = reverse('pyfb-rating:pyfb_rating_customerdefaultrate_update', args=[customerdefaultrate.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class ProviderDefaultRateViewTest(TestCase):
    '''
    Tests for ProviderDefaultRate
    '''
    def setUp(self):
        self.client = Client()

    def test_list_providerdefaultrate(self):
        url = reverse('pyfb-rating:pyfb_rating_providerdefaultrate_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_providerdefaultrate(self):
        url = reverse('pyfb-rating:pyfb_rating_providerdefaultrate_create')
        data = {
            "r_rate": '1.5',
            "r_block_min_duration": 1,
            "r_minimal_time": 1,
            "r_init_block": '1.5',
            "status": "enabled",
            "p_ratecard": create_providerratecard().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_providerdefaultrate(self):
        providerdefaultrate = create_providerdefaultrate()
        url = reverse('pyfb-rating:pyfb_rating_providerdefaultrate_detail', args=[providerdefaultrate.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_providerdefaultrate(self):
        providerdefaultrate = create_providerdefaultrate()
        data = {
            "r_rate": '1.5',
            "r_block_min_duration": 1,
            "r_minimal_time": 1,
            "r_init_block": '1.5',
            "status": "enabled",
            "p_ratecard": 1,
        }
        url = reverse('pyfb-rating:pyfb_rating_providerdefaultrate_update', args=[providerdefaultrate.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)
