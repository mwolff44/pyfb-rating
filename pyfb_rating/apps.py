# -*- coding: utf-8
from django.apps import AppConfig


class PyfbRatingConfig(AppConfig):
    name = 'pyfb_rating'
